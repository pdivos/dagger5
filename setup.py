from setuptools import setup

setup(name='dagger5',
    version='0.0.dev0',
    description='Dagger5 server',
    url='',
    author='',
    author_email='',
    license='GNU AGPLv3',
    packages=['dagger5'],
    zip_safe=False,
    install_requires=[
        'flask==0.12.2',
        'flask_restful==0.3.6',
        'flask_script==2.0.6',
        'flask_migrate==2.1.1',
        'ZODB==5.5.1',
        'BTrees==4.4.1',
        'pycrypto==2.6.1',
    ],
    scripts=[
    ],
)