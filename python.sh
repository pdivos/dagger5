DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
docker build -t dagger5 . # > /dev/null
docker run -i -e CFG_PASSWORD=$CFG_PASSWORD --rm --network host -v $DIR/data:/usr/src/app/data dagger5 python -u "$@"