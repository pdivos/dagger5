# 2019-08-14

After 6m of pause I am abandoning dagger4 and starting a new dagger5 project by greatly simplifying the design, here are the simplifications:
 - no need to expose functions through decorators like DFunction or DLambda, every public function is callable
 - no concept of callable hash, no dealing with source code changes
 - no concept of code repository
 - no multilingual support, just python
 - no multilingual serialization, just pickle
 - single threaded server serving a single repository, no separate worker process.
 - dagger5 server is a pip package, imported into the business logic repo worker through pip install
 - no websocket, server is accessible through rest
 - no diddle
 - no zodb, just bare files for completed stuff. running stuff such as queue and incomplete nodes only live in memory
 - almost like just a memoize pattern only with additional terminate-restart