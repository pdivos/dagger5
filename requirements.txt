flask==0.12.2
flask_restful==0.3.6
flask_script==2.0.6
flask_migrate==2.1.1
ZODB==5.5.1
BTrees==4.4.1
pycrypto==2.6.1
