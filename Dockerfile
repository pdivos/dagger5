# run this from the root folder:
# docker build -t dagger-python3 --build-arg CFG_PASSWORD=$DAGGER_CFG_PASS -f pd/docker/dagger-python3/Dockerfile .
FROM python:3.7

RUN pip install --no-cache-dir flask==0.12.2
RUN pip install --no-cache-dir flask_restful==0.3.6
RUN pip install --no-cache-dir flask_script==2.0.6
RUN pip install --no-cache-dir flask_migrate==2.1.1
RUN pip install --no-cache-dir ZODB==5.5.1
RUN pip install --no-cache-dir BTrees==4.4.1
RUN pip install --no-cache-dir pycrypto==2.6.1

COPY . /usr/src/app
# RUN pip install /usr/src/app/
ENV PYTHONPATH="/usr/src/app/"
ENV PYTHONIOENCODING=UTF-8
WORKDIR /usr/src/app