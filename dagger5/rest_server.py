# import json, os, urllib.request, urllib.parse, urllib.error
# from copy import deepcopy
from flask import Flask, request, render_template, send_from_directory, session
# from flask_httpauth import HTTPBasicAuth
# import pandas as pd
# import numpy as np

import os
# path = os.path.dirname(os.path.abspath(__file__)) + '/flask-templates/'
# app = Flask(__name__, template_folder = path, static_url_path = path)
# auth = HTTPBasicAuth()
# cache = None # bit hacky, but it will be set in start()

# @auth.verify_password
# def verify_pw(username, password):
#     from tools.config import cfg_get
#     users = cfg_get('flask_users')
#     if username not in users:
#         return False
#     return password == users[username]

# @app.route("/stop")
# def stop():
#     func = request.environ.get('werkzeug.server.shutdown')
#     assert func is not None, 'Not running with the Werkzeug Server'
#     func()
#     return "stopped"

# @app.route("/removeAll")
# def removeAll():
#     cache.removeAll()
#     return "ok"

# def xmp(s):
#     return "<xmp style='display: inline; white-space: pre; word-break: normal; word-wrap: normal;'>" + s + "</xmp>"

# def dcall_to_link(dcall, host = ''):
#     assert is_DCall(dcall)
#     dcall_key = dcall.to_key()
#     args = [ a for a in dcall.args ]
#     if len(args) > 0 and is_int(args[0]) and args[0] > 1000000000000:
#         args[ 0 ] = to_str(args[ 0 ])
#     args = [ value_to_link(a) for a in args ]
#     func = "<a href='" + host + "/getDCall?dcall=" + urllib.parse.quote(dcall_key) + "'>" +dcall.func + "</a>"
#     return func + "(" + ', '.join(args) + ")"

# html_escape_table = {
#     "&": "&amp;",
#     '"': "&quot;",
#     "'": "&apos;",
#     ">": "&gt;",
#     "<": "&lt;",
# }

# def html_escape(text):
#         """Produce entities within text."""
#         return "".join(html_escape_table.get(c,c) for c in text)

# def value_to_link(value, max_size = 100):
#     "value might be a primitive or a DVRef"
#     if is_DVRef(value):
#         vref = value
#         value = cache.value.resolveVRef(vref)
#         value_str = str(value)
#         if len(value_str) > max_size:
#             value_str = html_escape(value_str[ 0:max_size ])
#             value_str += "<a href='/getvalue_html?id=" + vref + "'>" + "..." + "</a>"
#         else:
#             value_str = html_escape(value_str)
#         return value_str
#     else:
#         return str(value)
    
# def render_index(body):
#     return render_template('index.html', body = body, title = cache.name())

# def render_dcall(**argv):
#     return render_template('dcall.html', **argv)
    
# @app.route("/")
# @auth.login_required
# def getFuncs():
#     tds = Tds([ "dcalls", "rti_keys" ], [])
#     for func in cache.node.getFuncs():
#         row = {}
#         row[ "dcalls" ] = '<a href="/getDCalls?func='+func+'">'+func+'</a>'
#         row[ "rti_keys" ] = '<a href="/getDCallsLast?func='+func+'">'+func+'</a>'
#         tds.append_row(row)
#     body = to_html(tds)
#     return render_index(body)

# @app.route("/getDCallsLast")
# def getDCallsLast():
#     tds = Tds([ "last", "all" ], [])
#     func = request.args.get("func", default = None)
#     res = cache.node.getDCallsLastByFunc(func)
#     for dcall in res:
#         row = { "last": dcall_to_link(dcall) }
#         row[ 'all' ] = '<a href=/getDCallsAllRuntime?dcall=' + urllib.parse.quote(dcall.to_key()) + '>all</a>'
#         tds.append_row(row)
#     body = to_html(tds)
#     return render_index(body)

# def render_dcalls(dcalls):
#     tds = Tds([ "chk", "status", "call", "value" ], [])
#     for r in dcalls:
#         dcall = DCall.from_key(r[ "dcall" ])
#         status = r[ "status" ]
#         value = r[ "value" ]
#         dcall_key = dcall.to_key()
#         row = { "call": dcall_to_link(dcall), "status": status, "value": "" }
#         row[ "chk" ] = "<input type='checkbox' name='tds' value='" + urllib.parse.quote(dcall_key) + "'>"
#         if status == DStatus.Succeeded:
#             row[ "value" ] = value_to_link(value)
#         tds.append_row(row)
#     body = ""
#     body += "<form action='/form_action' method='post' target='_blank'>"
#     body += to_html(tds)
#     body += "<input type='button' value='select all' onclick=\"(function() {var aa= document.getElementsByTagName('input'); for (var i =0; i < aa.length; i++){if (aa[i].type == 'checkbox') aa[i].checked = true;}})()\">"
#     body += "<input type='submit' name='submit' value='remove'>"
#     body += "<input type='submit' name='submit' value='reinit'>"
#     body += "</form>"
#     return render_index(body)

# @app.route("/getDCallsAllRuntime")
# def getDCallsAllRuntime():
#     dcall_key = request.args.get("dcall")
#     dcall_key = urllib.parse.unquote(dcall_key) # apparently this is done by flask automatically so no need to call again.
#     dcall = DCall.from_key(dcall_key)
#     res = cache.node.getDCallsAllRuntime(dcall)
#     return render_dcalls(res)

# @app.route("/getDCalls")
# def getDCalls():
#     func = request.args.get("func", default = None)
#     res = cache.node.getDCalls(by='func',key=func)
#     return render_dcalls(res)

# @app.route("/insertIfNotExists")
# def insertIfNotExists():
#     tds = Tds([ "k", "v" ], [])
#     tds.append_row({ "k": "func", "v": "<input type=text name=func></input>" })
#     tds.append_row({ "k": "args", "v": "<input type=text name=args></input>" })
#     body = ""
#     body += "<form action='/form_insertIfNotExists' method='post' target='_blank'>"
#     body += to_html(tds)
#     body += "<input type='submit' value='submit'>"
#     body += "</form>"
#     return render_index(body)

# @app.route("/form_insertIfNotExists", methods=['GET', 'POST'])
# def form_insertIfNotExists():
#     func = request.form[ "func" ] if 'func' in request.form else request.args.get('func')
#     args = request.form[ "args" ] if 'args' in request.form else request.args.get('args')
#     args = args.split(',')
#     for ii in range(len(args)):
#         a = args[ ii ].strip()
#         if ii == 0:
#             try:
#                 a = to_int(a)
#             except:
#                 try:
#                     a = str_to_primitive(a)
#                 except:
#                     a = cache.value.ensureVRef(a)
#         else:
#             try:
#                 a = str_to_primitive(a)
#             except:
#                 a = cache.value.ensureVRef(a)
#         args[ ii ] = a
#     dcall = DCall(func, args)
#     cache.node.insertIfNotExists(dcall)
#     body = "Done: insertIfNotExists(" + dcall_to_link(dcall) + ")"
#     return render_index(body)

# @app.route("/getDCall")
# def getDCall():
#     import tools.HTML as HTML
#     dcall_key = request.args.get("dcall")
#     dcall_key = urllib.parse.unquote(dcall_key) # apparently this is done by flask automatically so no need to call again.
#     dcall = DCall.from_key(dcall_key)
#     resp = cache.node.getDCall(dcall)
#     if resp is None:
#         return repr(dcall) + " doesn't exist"
#     # try get prev / next dcall
#     dcall_prev = None
#     dcall_next = None 
#     if len(dcall.args) > 0 and type(dcall.args[0]) in [int,int]:
#         runtime = dcall.args[0]
#         runtime_prev = cache.node.prevRuntime(dcall)
#         runtime_next = cache.node.nextRuntime(dcall)
#         dcall_prev = DCall(dcall.func, [dcall.args[i] if i > 0 else runtime_prev for i in range(len(dcall.args))]) if runtime_prev is not None else None
#         dcall_next = DCall(dcall.func, [dcall.args[i] if i > 0 else runtime_next for i in range(len(dcall.args))]) if runtime_next is not None else None
#     argv = { 'dcall': dcall_to_link(dcall),
#                 'dcall_key': urllib.parse.quote(dcall_key),
#                 'status': resp[ "status" ] }
#     if dcall_prev is not None:
#         argv['dcall_prev'] = dcall_to_link(dcall_prev)
#     if dcall_next is not None:
#         argv['dcall_next'] = dcall_to_link(dcall_next)
#     hist = resp[ "hist" ]
#     if len(hist) > 0:
#         hist = [ h.split(',') for h in hist ]
#         for h in hist:
#             h[0] = to_str(int(h[0].split('.')[0]))
#         hist = [ h[0] + "\t" + ','.join(h[1:]) for h in hist] 
#         hist = HTML.list(hist).replace("<UL>\n", "<UL>")
#         argv[ 'log' ] = hist
#     for k in [ "deps", "parent_deps" ]:
#         deps = resp[ k ]
#         if len(deps) > 0:
#             deps = [ dcall_to_link(DCall.from_key(d)) for d in deps ]
#             deps = HTML.list(deps).replace("<UL>\n", "<UL>")
#             argv[ 'children' if k == 'deps' else 'parents' ] = deps
#     if resp[ "status" ] in [ "SUCCEEDED", "FAILED" ]:
#         value = resp[ "value" ]
#         if is_DVRef(value):
#             argv[ 'dvref' ] = value
#         else:
#             argv[ 'value' ] = str(value)
#         for k in [ "stdout", "stderr", "source" ]:
#             argv[ k ] = value_to_link(resp[ k ], 10000)
#     return render_dcall(**argv)

# @app.route("/file")
# def file():
#     fname = request.args.get('f')
#     assert fname in ['handsontable.full.css','handsontable_main.css','jquery-3.2.1.slim.min.js','handsontable.full.js','ir-black.min.css','highlight.min.js','python.min.js','ibm_vga8.woff']
#     return send_from_directory(path, fname)

# @app.route("/getvalue_raw")
# def getvalue_raw():
#     valueRef = DVRef(request.args.get("id"))
#     key = valueRef[ 1: ]
#     if valueRef[ 0 ] == '@': # if starts with @ then it's a JSONable object
#         raw = cache.value.dKeyValueCache.get( key )
#         jsonable = json.loads( raw )
#         return xmp(json.dumps(jsonable, indent=4))
#     elif valueRef[ 0 ] == '$': # if starts with $ then it's a pd.DataFrame
#         return valueRef + " is a postgres dataframe, cannod display raw"
#     else:
#         return 'vref should start with @ or $, instead have: ' + valueRef

# def dataframe_to_html(df):
#     return df.to_html()
#     # all of the below html dataframe stuff is commented out because I don't use it and it's buggy so I didn't bother fixing
#     import pandas as pd
#     data = df.where((pd.notnull(df)), None).as_matrix().tolist()
#     data = [[to_str(e) if type(e)==pd.Timestamp else e for e in r] for r in data]
#     cols = df.columns.values.tolist()
#     html = """
# <html><head>
# <!--
# <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
# <script src="https://docs.handsontable.com/pro/1.14.3/bower_components/handsontable-pro/dist/handsontable.full.min.js"></script>
# -->
# <script src="/file?f=jquery-3.2.1.slim.min.js"></script>
# <script src="/file?f=handsontable.full.js"></script>

# <script>
# $(document).ready(function () {
# $("#grid").handsontable({
# colHeaders: JSON.parse('""" + json.dumps(cols) + """'), //["", "Kia", "Nissan", "Toyota", "Honda"],  
# columnSorting: true,
# sortIndicator: true,
# filters: true,
# dropdownMenu: true,
# contextMenu: true,
# autoRowSize: true,
# manualColumnMove: true,
# manualRowMove: true,  
# copyPaste: true,
# columnHeadersClipboard: true,
# });
# var data = JSON.parse(`""" + json.dumps(data,indent=4) + """`);
# //[
# //  ["2008", 10, 11, 12, 13],
# //  ["2009", 20, 11, 14, 13],
# //  ["2010", 30, 15, 12, 13]
# //];
# $("#grid").handsontable("loadData", data);
# });
# </script>
# <!--
# <link rel="stylesheet" type="text/css" href="http://docs.handsontable.com/pro/bower_components/handsontable-pro/dist/handsontable.full.min.css">
# <link rel="stylesheet" type="text/css" href="http://handsontable.com/static/css/main.css">
# -->
# <link rel="stylesheet" type="text/css" href="/file?f=handsontable.full.css">
# <link rel="stylesheet" type="text/css" href="/file?f=handsontable_main.css">

# </head><body style="margin: 20px;">
# <div id="grid" class="dataTable"></div>
# </body></html>
#     """
#     return html
#     #assert isinstance(df, pd.DataFrame)
#     #return df.to_html()

# @app.route("/getvalue_html")
# def getvalue_html():
#     vref = DVRef(request.args.get("id"))
#     value = cache.value.resolveVRef(vref)
#     if is_string(value):
#         value_str = value
#     elif type(value) == pd.DataFrame:
#         value_str = dataframe_to_html(value)
#     elif type(value) == np.ndarray:
#         value_str = pd.DataFrame(value).to_html()
#     else:
#         try:
#             value_str = xmp(json.dumps(value, indent=4))
#         except:
#             value_str = str(value)
#     return value_str

# @app.route("/action")
# def reinit():
#     dcall_key = request.args.get("dcall")
#     action = request.args.get("action")
#     dcall = DCall.from_key(dcall_key)
#     if action == 'remove':
#         cache.node.remove(dcall_key)
#     elif action == 'reinit':
#         cache.node.reinit(dcall)
#     else:
#         return "error: unknown action"
#     return "OK"

# @app.route("/form_action", methods=['GET', 'POST'])
# def form_action():
#     dcall_keys = [ urllib.parse.unquote(d) for d in request.form.getlist('tds') ]
#     action = request.form['submit']
#     results = {}
#     for dcall_key in dcall_keys:
#         dcall = DCall.from_key(dcall_key)
#         if action == 'remove':
#             results[ dcall_key ] = cache.node.remove(dcall_key)
#         elif action == 'reinit':
#             cache.node.reinit(dcall)
#             results[ dcall_key ] = True
#         else:
#             raise Exception('unknown action')
#     tds = Tds([ "dcall_key", "result" ], [])
#     for dcall_key, r in results.items():
#         tds.append_row({ 'dcall_key': str(dcall_key), 'result': results[ dcall_key ] })
#     return render_index('<h3>' + action + '</h3>' +  to_html(tds))

app = Flask(__name__)
from server import Dagger5Server

server = Dagger5Server()

@app.route("/ok")
def ok():
    return "OK:"+str(request.args)

@app.route("/err")
def err():
    raise Exception("OK")

def start(port):
    app.run(host = '0.0.0.0', port = port, threaded = True)

if __name__ == '__main__':
    import sys
    port = 8888
    if len(sys.argv) > 1:
        port = int(sys.argv[1])
    start(port)