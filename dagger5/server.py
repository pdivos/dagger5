import os
from dagger5.core import DStatus, DCall
from dagger5.utils import log
from dagger5.dnodedb import DNodeDb

def debug(fun):
    def fun_new(*args, **kwargs):
        callstr = fun.__name__ + '(' + ", ".join([repr(a) for a in (args[1:] if (len(args)>0 and type(args[0]) is Dagger5Server) else args)]) + ')'
        from dagger5 import Globals
        _debug = Globals.get_loglevel() > 0
        try:
            if _debug: log(callstr)
            r = fun(*args, **kwargs)
            if r is not None:
                if _debug: log('\t=> ' + repr(r))
            # if '__drefstore_s2c__' in kwargs and kwargs['__drefstore_s2c__'] is not None:
            #     __drefstore_s2c__ = kwargs['__drefstore_s2c__']
            #     if len(__drefstore_s2c__['new_keyvalues']) + len(__drefstore_s2c__['required_keys']) > 0:
            #         if _debug: log('__drefstore_s2c__: ' + repr(__drefstore_s2c__))
            return r
        except Exception as e:
            if _debug: log('Exception: ' + callstr + ' => ' + repr(e))
            raise
    return fun_new

from queue import Queue, Empty

class Dagger5Server(object):
    def __init__(self, cache):
        """
        cache: one of
            'file:[/path/to/folder]'
            'zodb:[/path/to/folder]'
            'memory'
        """

        self.db = DNodeDb(cache)
        self.queue = Queue()
        self.is_running = False

    def serve_forever(self, stop_on_done = False):
        assert not self.is_running
        self.is_running = True
        from dagger5.worker import worker_loop
        worker_loop(self, stop_on_done = stop_on_done, tee = True)
        self.shutdown()
        self.is_running = False

    def shutdown(self):
        self.db.shutdown()

    @debug
    def get_work(self, timeout):
        if timeout is None:
            dcall = self.queue.get()
        elif timeout == 0:
            try:
                dcall = self.queue.get(False)
            except Empty:
                return None
        else:
            try:
                dcall = self.queue.get(True, timeout)
            except Empty:
                return None
        node = self.db.get_node_executing(dcall)
        dnode_id = node['id']
        self.db.update_status(dnode_id, DStatus.RUNNING)
        return dcall

    @debug
    def call(self, callee, caller = None):
        """
        calls a DCall
            if callee is available then just return,
            otherwise suspend caller and put callee on queue
        caller: the caller dcall that is running and that is calling callee
        callee: the callee whose value is requested
        """
        assert type(callee) is DCall and (caller is None or type(caller) is DCall)

        # try to select suceeded callee node
        callee_node = self.db.get_node_completed(callee)
        if callee_node is None:
            # if no completed found, try select an executing node
            callee_node = self.db.get_node_executing(callee)
        if callee_node is None:
            # if callee not completed and not executing, then must be created
            # and put on work queue
            callee_node = self.db.insert_node_executing(callee)
            self.queue.put(callee)

        if caller is not None:
            caller_node = self.db.get_node_executing(caller)
            assert caller_node['dstatus'] == DStatus.RUNNING, caller_node

            if callee_node['dstatus'] != DStatus.COMPLETED:
                # if callee not COMPLETED, then mark caller as WAITING
                # when callee finishes caller gets put on the queue again for second pass, see complete
                self.db.update_status(caller_node['id'], DStatus.WAITING)
            # register dep to make sure we know that there was a dep here
            # calling register_dep multiple times is OK
            self.db._graph_executing.register_dep(callee_node['id'], caller_node['id'])

        elif callee_node['dstatus'] == DStatus.WAITING and caller is None:
            # if status is WAITING and caller is None then external client called,
            # might be interested in what the node is waiting for
            callee_node['waiting_on'] = []
            for cid in self.db._graph_executing.callees_deep(callee_node['id']):
                node = self.db.get_node_executing_by_id(cid)
                if node is not None and node['dstatus'] in [DStatus.READY,DStatus.RUNNING]:
                    callee_node['waiting_on'].append({k:node[k] for k in ['dcall','dstatus']})
        return callee_node

    @debug
    def call_multiple(self, callees, caller = None):
        """
        multiple calls in one go
        doesn't return results of individual calls, instead:
        returns True if all deps are in and False othersiwe
        """
        if caller is not None:
            assert type(caller) is DCall
            caller_node = self.db.get_node_executing(caller)
            caller_id = caller_node['id']
            assert caller_node['dstatus'] == DStatus.RUNNING
        any_missing = False
        for callee in callees:
            assert type(callee) is DCall
            callee_node = self.call(callee, caller)
            if callee_node['dstatus'] != DStatus.COMPLETED:
                any_missing = True
                if caller is not None:
                    # check caller went into waiting state
                    assert caller_node['dstatus'] == DStatus.WAITING
                    # put caller back manually into RUNNING state so we can do self.call() with the next callee
                    self.db.update_status(caller_id, DStatus.RUNNING, force = True)
        # if we ever put caller back to RUNNING make sure to have it as WAITING in the end
        if any_missing and caller is not None:
            self.db.update_status(caller_id, DStatus.WAITING, force = True)
        return not any_missing

    @debug
    def complete(self, dcall, dnoderesult):
        """
        sets a RUNNING dnodekeyrunning to COMPLETED
        """
        from dagger5.core import DNodeResult
        assert type(dcall) is DCall and type(dnoderesult) is DNodeResult
        node_running = self.db.get_node_executing(dcall)
        id_executing = node_running['id']
        assert node_running['dstatus'] == DStatus.RUNNING, node_running['dstatus']

        # get deep dcallable hash from the nodes that this one called
        for calleeid in self.db.executing_callee_ids(id_executing):
            callee = self.db.get_node_completed(calleeid)
            assert callee['dstatus'] == DStatus.COMPLETED, callee

        # insert node into completed nodes, remove from executing
        children = self.db._graph_executing.callees(id_executing)
        id_completed = self.db.insert_node_completed(dcall, dnoderesult, children)
        self.db.remove_node_executing(id_executing)

        # in executing graph rename executing id to completed id
        # we need this because some executing nodes may have the completed as child and need to update for them
        self.db._graph_executing.merge(id_executing,id_completed)

        # move all completed_node -> children links from executing graph to completed
        # because both ends of those links are now completed
        calleeids = [calleeid for calleeid in self.db.executing_callee_ids(id_completed)]
        for calleeid in calleeids:
            self.db._graph_executing.remove_dep(calleeid, id_completed)
            # self.db._graph_completed.register_dep(calleeid, id_completed)

        # we go through all the guys that were waiting for this
        # if this was the last one anyone was waiting for then we set that one to READY
        callers = self.db.update_waiting_callers_to_ready(id_completed)
        for caller in callers:
            self.queue.put(caller)

    @debug
    def remove(self, callee):
        """
        """
        assert type(callee) is DCall

        # try to select suceeded callee node
        return self.db.remove_node_completed(callee)

    def get_completed_nodes(self, ts = None, module = None, func = None, include_failed = False):
        return self.db.get_nodes_completed(ts=ts, module=module, func=func, include_failed=include_failed)

    def get_completed_node_by_id(self, id):
        return self.db.get_node_completed(id)
