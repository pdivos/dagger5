from dagger5.core import DCall, DNodeResult, DRef, DException, DMissingDep
from dagger5 import Globals

def log(*args):
    from dagger5.utils import log as _log
    if Globals.get_loglevel() > 0:
        _log(*args)

def _ensure(dagger5server, success, value, stdout, stderr, exception, stacktrace):
    "serilaize results"
    try:
        value = dagger5server.db.dref_ensure(value)
        stdout = dagger5server.db.dref_ensure(stdout)		
        stderr = dagger5server.db.dref_ensure(stderr)	
        stacktrace = stacktrace # don't serialize stacktrace here, will be serialized once wrapped in dexception	
    except BaseException as e:
        # if a serialization error happens then overwrite any earlier exception/stacktrace
        exception = e
        import traceback
        stacktrace = traceback.format_exc()
        success = False
        value = None
    return (success, value, stdout, stderr, exception, stacktrace)

def _run(dagger5server, fun, args, tee = False):
    # seems a bit messy to wrap run in a _run
    # but we need to handle exception occurring during serialization
    # which is common if user is trying to serialize unknown type
    from dagger5.utils import run
    
    success, value, stdout, stderr, exception, stacktrace = run(fun, args, tee = tee)
    success, value, stdout, stderr, exception, stacktrace = _ensure(dagger5server, success, value, stdout, stderr, exception, stacktrace)
    # success, stacktrace = _check_unused_children(success, stacktrace)
    return (success, value, stdout, stderr, exception, stacktrace)

import enum
class Status(enum.IntEnum):
    SUCCEED = 0
    EXCEPTION = 1
    MISSINGDEP = 2
    
def execute_dnode(dagger5server, dcall, tee = False):
    """
      executes a DNodeKeyRunning
      dnodekeyrunning: dnodekeyrunning to execute to
      olddeps: list of existing evaled deps of the node, needed to check if Eval can be called
      cache: used by DFunction to access dep values
      returns a DNode which contains the results of the run
    """
    assert type(dcall) is DCall
    import importlib
    try:
        module = importlib.import_module(dcall.module)
    except ModuleNotFoundError:
        stdout = stderr = None
        stacktrace = 'Unable to import module: ' + dcall.module
        dagger5server.complete(dcall, DNodeResult(False, stacktrace, stdout, stderr))
        return Status.EXCEPTION

    try:
        fun = getattr(module, dcall.func)
    except AttributeError:
        stdout = stderr = None
        stacktrace = 'Unable to find function: ' + dcall.func + ' in module ' + dcall.module
        dagger5server.complete(dcall, DNodeResult(False, stacktrace, stdout, stderr))
        return Status.EXCEPTION

    assert callable(fun)
    args = [dagger5server.db.dref_resolve(a) for a in dcall.args]
    assert type(args) is list
    # success, value, stdout, stderr, exception, stacktrace = run(fun, args, tee = tee)
    success, value, stdout, stderr, exception, stacktrace = _run(dagger5server, fun, args, tee = tee)

    if success == True:
        dagger5server.complete(dcall, DNodeResult(True, value, stdout, stderr))
        return Status.SUCCEED
    elif success == False:
        # DException and DMissingDep exceptions are special to dagger and need to be handled specially
        if type(exception) is DException:
            # client node code has the opportunity to catch and handle dagger exceptions
            # but if it doesn't, we re-throw it in this way and just add the current node in it's list of nodes
            stacktrace = exception.stacktrace
            dcalls = [el for el in exception.dcalls]
            dcalls.append(Globals.get_dcall())
            dexception = DException(stacktrace, dcalls)
            dexception = dagger5server.db.dref_ensure(dexception)
            dagger5server.complete(dcall, DNodeResult(False, dexception, stdout, stderr))
            return Status.EXCEPTION
        elif type(exception) is DMissingDep:
            # missing dep is raised on a missing dep, we just need to return this
            # server already knows, no need to send any additional request
            return Status.MISSINGDEP
        else:
            # all other exceptions happening during regular business are wrapped in a DExcpetion
            # we don't keep the exception type, just the stack trace as a string because
            # otherwise it would be hard to be cross-platform
            assert exception is not None and type(stacktrace) is str
            try:
                exception_handler = Globals.get_exception_handler()
                if exception_handler is not None:
                    exception_handler(stacktrace)
            except:
                # hide any exceptions happening from exception_handler because we got nowhere to report to
                pass
            dexception = DException(stacktrace, [Globals.get_dcall()])
            dexception = dagger5server.db.dref_ensure(dexception)
            dagger5server.complete(dcall, DNodeResult(False, dexception, stdout, stderr))
            return Status.EXCEPTION
    else:
        assert False, success

def worker_loop(dagger5server, stop_on_done = False, sleep_secs = 1, tee = False, graceful_stop_on_SIGINT = True):
    running = [True]

    if graceful_stop_on_SIGINT:
        import signal
        original_signal_handler = signal.getsignal(signal.SIGINT)
        def my_signal_handler(sig, frame):
                log('SIGINT received, stopping gracefully')
                running[0] = False
        signal.signal(signal.SIGINT, my_signal_handler)

    Globals.set_dagger5server(dagger5server)
    waiting_for_work_logged = False
    while running[0]:
        resp = dagger5server.get_work(0 if stop_on_done else sleep_secs)
        if resp is None:
            if stop_on_done:
                log('no more work, exiting')
                break
            else:
                if not waiting_for_work_logged:
                    log('waiting for work...')
                waiting_for_work_logged = True
                continue
        waiting_for_work_logged = False
        dcall = resp
        assert type(dcall) is DCall
        Globals.set_dcall(dcall)
        log('starting to execute', repr(dcall))
        status = execute_dnode(dagger5server, dcall, tee=tee)
        Globals.set_dcall(None)
        # if status == Status.EXCEPTION:
            # if on_failed_dcallback is not None and dnodekeyrunning.func != on_failed_dcallback:
            #     from dagger5.utils import now
            #     client.call(DNodeKeyRunning(DFunType.EVENT, on_failed_dcallback, 
            #                      [dnodekeyrunning, value.stdout, value.stderr, value.source, value.stacktrace], now()))
        log('\t=> ' + repr(status))
    Globals.set_dagger5server(None)

    if graceful_stop_on_SIGINT:
        signal.signal(signal.SIGINT, original_signal_handler)
        log('SIGINT received, stopped gracefully')
