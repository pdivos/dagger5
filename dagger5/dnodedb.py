import os
import BTrees.OLBTree, BTrees.OOBTree
import persistent.mapping
from dagger5.core import DStatus, DCall, DRef, DNodeResult
from dagger5.serializer import serialize, deserialize
from dagger5.utils import log
from dagger5.utils import write_file, read_file, exists_file, remove_file

class DNodeGraph():
    """
    stores call graph:
        caller dnode_id -> set of callee dnode_id
        callee dnode_id -> set of caller dnode_id
    """
    def __init__(self):
        from BTrees.OOBTree import OOBTree
        # set of callees that caller was calling
        self._caller_to_callees = OOBTree()
        # set of callers that were calling callee
        self._callee_to_callers = OOBTree()

    def _check(self):
        "self-check"
        for caller, callees in self._caller_to_callees.items():
            assert len(callees) > 0, caller
            for callee in callees:
                assert callee in self._callee_to_callers, callee
                assert caller in self._callee_to_callers[callee], caller
                assert caller != callee
        for callee, callers in self._callee_to_callers.items():
            assert len(callers) > 0, callee
            for caller in callers:
                assert caller in self._caller_to_callees, caller
                assert callee in self._caller_to_callees[caller], callee
                assert caller != callee

    def callers(self, calleeid):
        return set(self._callee_to_callers.get(calleeid,set()))
        
    def callees(self, callerid):
        return set(self._caller_to_callees.get(callerid,set()))

    def callees_deep(self, callerid):
        "returns deep set of callees by traversing the whole graph"
        q = [callerid]
        callees_deep = set([callerid])
        while len(q):
            callerid = q.pop()
            for c in self.callees(callerid):
                if c not in callees_deep:
                    callees_deep.add(c)
                    q.append(c)
        return callees_deep
        
    def register_dep(self, calleeid, callerid):
        """
        registers dependencies
        """
        from BTrees.OOBTree import OOTreeSet
        if callerid not in self._caller_to_callees:
            self._caller_to_callees[callerid] = OOTreeSet()
        self._caller_to_callees[callerid].add(calleeid)
        if calleeid not in self._callee_to_callers:
            self._callee_to_callers[calleeid] = OOTreeSet()
        self._callee_to_callers[calleeid].add(callerid)

    def remove_dep(self, calleeid, callerid):
        self._caller_to_callees[callerid].remove(calleeid)
        if len(self._caller_to_callees[callerid]) == 0:
            del self._caller_to_callees[callerid]
        self._callee_to_callers[calleeid].remove(callerid)
        if len(self._callee_to_callers[calleeid]) == 0:
            del self._callee_to_callers[calleeid]

    def merge(self, id1, id2):
        """
        merges id1 -> id2
        any nodes pointing to/from id1 should point to/from id2
        then id1 should be removed
        """
        if id1 == id2:
            return
        from BTrees.OOBTree import OOTreeSet
        if id1 in self._caller_to_callees:
            if id2 not in self._caller_to_callees:
                self._caller_to_callees[id2] = OOTreeSet()
            for c in self._caller_to_callees[id1]:
                self._callee_to_callers[c].remove(id1)
                self._callee_to_callers[c].add(id2)
                self._caller_to_callees[id2].add(c)
            del self._caller_to_callees[id1]
        if id1 in self._callee_to_callers:
            if id2 not in self._callee_to_callers:
                self._callee_to_callers[id2] = OOTreeSet()
            for c in self._callee_to_callers[id1]:
                self._caller_to_callees[c].remove(id1)
                self._caller_to_callees[c].add(id2)
                self._callee_to_callers[id2].add(c)
            del self._callee_to_callers[id1]

    def __repr__(self):
        r = "callee_to_callers: " + str({k:set([_ for _ in v]) for k,v in self._callee_to_callers.items()}) + "\n"
        r += "caller_to_callees: " + str({k:set([_ for _ in v]) for k,v in self._caller_to_callees.items()})
        return r

def test_DNodeGraph():
    g = DNodeGraph()
    g.register_dep(0,2)
    g._check()
    g.register_dep(1,2)
    g._check()
    g.register_dep(2,3)
    g._check()
    g.register_dep(2,4)
    g._check()

    g.register_dep(0,6)
    g._check()
    g.register_dep(6,3)
    g._check()
    g.register_dep(6,4)
    g._check()
    g.register_dep(6,5)
    g._check()

    g.merge(2,6)
    g._check()

    assert 3 in g._callee_to_callers[6]
    assert 4 in g._callee_to_callers[6]
    assert 5 in g._callee_to_callers[6]

    assert 0 in g._caller_to_callees[6]
    assert 1 in g._caller_to_callees[6]

    g.register_dep(7,0)
    g._check()

    g._check()

class DNodeDb():
    """
    maintains and persists nodes and dep graph
    """
    def __init__(self, cache):
        assert type(cache) is str
        self.storage_option = cache.split(":")[0]
        assert self.storage_option in ['memory','zodb','file'], cache
        if self.storage_option in ['file','zodb']:
            self.cache_folder = cache.split(":")[1]
        from BTrees.OOBTree import OOBTree
        self._nodes_executing = OOBTree()   # executing node dref -> node
        self._graph_executing = DNodeGraph()    # caller: only from _nodes_executing, calle: from both _nodes_executing and _nodes_completed

        if self.storage_option in ['zodb','memory']:
            self._nodes_completed = OOBTree()   # completed node dref -> node
            # we got two caller -> callee graphs
            # _graph_executing
            # self._graph_completed = DNodeGraph()    # caller: only from _nodes_completed, callee: only from _nodes_completed
            # dref store contains global dref -> value
            self._drefstore = OOBTree()
        else:
            assert self.storage_option == 'file'

        if self.storage_option == 'memory':
            pass
        elif self.storage_option == 'zodb':
            cleanup_on_start = False
            if not os.path.exists(self.cache_folder):
                os.makedirs(self.cache_folder)
            zodb_fname = self.cache_folder + '/dagger5.zodb'
            from dagger5.myzodb import MyZodb
            self.my_zodb = MyZodb(zodb_fname, cleanup_on_start)
            # we only save the completed nodes and graph
            # executing stuff is not persisted
            attrs_to_bind = [
                '_nodes_completed',
                '_drefstore',
                # '_graph_completed._caller_to_callees',
                # '_graph_completed._callee_to_callers',
            ]
            self.my_zodb.bind(self, attrs_to_bind)
        elif self.storage_option == 'file':
            if not os.path.exists(self.cache_folder + '/nodes'):
                os.makedirs(self.cache_folder + '/nodes')
            if not os.path.exists(self.cache_folder + '/drefs'):
                os.makedirs(self.cache_folder + '/drefs')
        else:
            assert False, self.storage_option

    def shutdown(self):
        if self.storage_option == 'zodb':
            self.my_zodb.commit()
            self.my_zodb.close()
        else:
            assert self.storage_option in ['file','memory'], self.storage_option

    def drefstore_insert(self, key_values):
        if self.storage_option in ['zodb','memory']:
            assert type(key_values) is dict
            for k, v in key_values.items():
                assert type(k) is DRef and type(v) is bytes
                if k not in self._drefstore:
                    self._drefstore[k] = v
        else:
            assert self.storage_option == 'file'
            for k, v in key_values.items():
                assert type(k) is DRef and type(v) is bytes
                path = self.cache_folder + '/drefs/' + k.hex()
                if not exists_file(path):
                    write_file(path, v)
    
    def drefstore_contains(self, k):
        if self.storage_option in ['zodb','memory']:
            assert type(k) is DRef
            return k in self._drefstore
        else:
            assert self.storage_option == 'file'
            path = self.cache_folder + '/drefs/' + k.hex()
            return exists_file(path)

    def drefstore_get(self, k):
        if self.storage_option in ['zodb','memory']:
            assert type(k) is DRef
            return self._drefstore[k]
        else:
            assert self.storage_option == 'file'
            path = self.cache_folder + '/drefs/' + k.hex()
            return read_file(path)

    def dref_ensure(self, obj):
        """
        ensure:
            primitive -> primitive
            DRef      -> DRef
            object    -> DRef
        """
        from dagger5.core import is_primitive
        if is_primitive(obj) or type(obj) is DRef:
            return obj
        else:
            obj = serialize(obj)
            dref = DRef.from_buf(obj)
            if not self.drefstore_contains(dref):
                self.drefstore_insert({dref: obj})
            return dref

    def dref_resolve(self, dref):
        """
        resolve:
            primitive -> primitive
            DRef      -> object
            object    -> Exception
        """
        from dagger5.core import is_primitive
        if type(dref) is DRef:
            return deserialize(self.drefstore_get(dref))
        else:
            assert is_primitive(dref), 'unknown type: ' + str(type(dref))
            return dref

    def get_node_executing(self, dcall):
        """
        tries to find a node among the READY,RUNNING,WAITING nodes

        the principle here is that a running node qualifies to be the same only if all dnodekeyrunning fields are the same
        we have no way of knowing if two running dcalls that only differ in dcommitset would end up being the same node
            before the run completes, so we just let them run and upon completion merge them if they are the same
            see merign part in complete()
        """
        assert type(dcall) is DCall, str(type(dcall)) + ':' + repr(dcall)
        found = []
        for _id, node in self._nodes_executing.items():
            if node['dstatus'] in [DStatus.READY, DStatus.WAITING, DStatus.RUNNING]:
                if node['dcall'] != dcall: continue
                found.append(_id)
            else:
                assert node['dstatus'] == DStatus.COMPLETED
        assert len(found) in [0,1], str(dcall) + ", " + str(found)
        if len(found) == 0:
            return None
        elif len(found) == 1:
            return self._nodes_executing[found[0]]
        else:
            assert False, str(dcall) + ", " + str(found)

    def get_node_executing_by_id(self, id):
        "returns executing node by node id or None if no such executing node"
        return self._nodes_executing.get(id)

    def insert_node_completed(self, dcall, dnoderesult, children):
        assert type(dcall) is DCall and type(dnoderesult) is DNodeResult
        assert type(children) is set
        for c in children: assert type(c) is DRef
        _id = DRef.from_buf(serialize(dcall))
        node = {
            'id':_id,
            'dcall': dcall,
            'result': dnoderesult,
            'children': children,
            'dstatus': DStatus.COMPLETED,
        }
        if self.storage_option in ['memory','zodb']:
            if _id not in self._nodes_completed:
                self._nodes_completed[_id] = node
        else:
            assert self.storage_option == 'file'
            path = self.cache_folder + '/nodes/' + _id.hex()
            if not exists_file(path):
                write_file(path, serialize(node))
        return _id

    def get_node_completed(self, dcall_or_id):
        """
        tries to find a node dnodekeyrunning among the COMPLETED nodes

        the main principle here is that a COMPLETED node is the same as the given dnodekeyrunning if:
            has same args and ts
            the deep dcallable hashes of the node are all within the dcallable hashes of the reposet of the dnodekeyrunning
                this ensures that in this reposet the dnodekeyrunning would call the same child dcalls and eventually return the same value
            no need to check func (aka. dcallable name) and dcallable hash itself because deep dep hash 'contains' those
        """
        if type(dcall_or_id) is DCall:
            _id = DRef.from_buf(serialize(dcall_or_id))
        else:
            assert type(dcall_or_id) is DRef
            _id = dcall_or_id
        found = None
        if self.storage_option in ['memory','zodb']:
            found = self._nodes_completed.get(_id)
        else:
            assert self.storage_option == 'file'
            path = self.cache_folder + '/nodes/' + _id.hex()
            if exists_file(path):
                found = deserialize(read_file(path))
        return found
    
    def remove_node_completed(self, dcall_or_id):
        """
        tries to rempve a node dnodekeyrunning from the COMPLETED nodes

        the main principle here is that a COMPLETED node is the same as the given dnodekeyrunning if:
            has same args and ts
            the deep dcallable hashes of the node are all within the dcallable hashes of the reposet of the dnodekeyrunning
                this ensures that in this reposet the dnodekeyrunning would call the same child dcalls and eventually return the same value
            no need to check func (aka. dcallable name) and dcallable hash itself because deep dep hash 'contains' those
        """
        if type(dcall_or_id) is DCall:
            _id = DRef.from_buf(serialize(dcall_or_id))
        else:
            assert type(dcall_or_id) is DRef
            _id = dcall_or_id
        if self.storage_option in ['memory','zodb']:
            if _id not in self._nodes_completed:
                return False
            del self._nodes_completed[_id]
            return True
        else:
            assert self.storage_option == 'file'
            path = self.cache_folder + '/nodes/' + _id.hex()
            if not exists_file(path):
                return False
            remove_file(path)
            return True
    
    def exists_node_completed(self, dcall_or_id):
        if type(dcall_or_id) is DCall:
            _id = DRef.from_buf(serialize(dcall_or_id))
        else:
            assert type(dcall_or_id) is DRef
            _id = dcall_or_id
        if self.storage_option in ['memory','zodb']:
            return _id in self._nodes_completed
        else:
            assert self.storage_option == 'file'
            path = self.cache_folder + '/nodes/' + _id.hex()
            return exists_file(path)

    def get_nodes_completed(self, ts = None, module = None, func = None, include_failed = False):
        """
        returns list of completed nodes based on the provided filters
        """
        nodes = []
        if self.storage_option in ['memory','zodb']:
            for node in self._nodes_completed.values():
                if not include_failed and not node['result'].is_success: continue
                if ts is not None and node['dcall'].ts != ts: continue
                if module is not None and node['dcall'].module != module: continue
                if func is not None and node['dcall'].func != func: continue
                nodes.append(node)
        else:
            assert self.storage_option == 'file'
            mypath = self.cache_folder + '/nodes/'
            _ids = [f for f in os.listdir(mypath) if os.path.isfile(os.path.join(mypath, f))]
            for _id in _ids:
                _id = DRef(bytearray.fromhex(_id))
                node = self.get_node_completed(_id)
                if not include_failed and not node['result'].is_success: continue
                if ts is not None and node['dcall'].ts != ts: continue
                if module is not None and node['dcall'].module != module: continue
                if func is not None and node['dcall'].func != func: continue
                nodes.append(node)
        return nodes

    def insert_node_executing(self, dcall):
        assert type(dcall) is DCall
        _id = DRef.from_buf(serialize(dcall))
        node = {
            'dcall':dcall,
            'dstatus':DStatus.READY,
            'id':_id,
        }
        self._nodes_executing[_id] = node
        return node
    
    def remove_node_executing(self, id):
        assert type(id) is DRef and id in self._nodes_executing
        del self._nodes_executing[id]

    def update_status(self, dnode_id, dstatus, force = False):
        """
        force=True: don't check previous status, good for manual tinkering
        """
        assert isinstance(dnode_id, DRef), dnode_id
        assert isinstance(dstatus, DStatus)
        dstatus_old = self._nodes_executing[dnode_id]['dstatus']
        if dstatus == DStatus.READY:
            assert dstatus_old is DStatus.WAITING or force
        elif dstatus == DStatus.RUNNING:
            assert dstatus_old is DStatus.READY or force
        elif dstatus == DStatus.WAITING:
            assert dstatus_old is DStatus.RUNNING or force
        elif dstatus == DStatus.COMPLETED:
            assert dstatus_old is DStatus.RUNNING or force
        else:
            assert False, dstatus
        self._nodes_executing[dnode_id]['dstatus'] = dstatus
        if dstatus == DStatus.COMPLETED:
            # remove dcommit because it makes no sense to have dcommitset in a completed node
            assert 'dcall' in self._nodes_executing[dnode_id]
            del self._nodes_executing[dnode_id]['dcall']
        self._nodes_executing[dnode_id] = self._nodes_executing[dnode_id] # trigger zodb _p_changed
                
    def update_node(self, dnode_id, v):
        assert type(dnode_id) is DRef
        self._nodes_executing[dnode_id].update(v)
        self._nodes_executing[dnode_id] = self._nodes_executing[dnode_id] # trigger zodb _p_changed

    # dnodekeyrunning graph related methods

    def update_waiting_callers_to_ready(self, dnode_id):
        """
        returns list of callers that were only waiting on this callee
        we go through all the guys that were waiting for this
        if this was the last one anyone was waiting for then we set that one to READY
        the returned list of callers need to be put on the work queue
        """
        assert type(dnode_id) is DRef
        callerids = []
        for callerid in self._graph_executing.callers(dnode_id):
            any_missing = False
            for calleeid in self._graph_executing.callees(callerid):
                if calleeid in self._nodes_executing:
                    any_missing = True
                else:
                    assert self.exists_node_completed(calleeid)
            if not any_missing:
                callerids.append(callerid)
        for callerid in callerids:
            assert callerid in self._nodes_executing
            self.update_status(callerid, DStatus.READY)
        callers = [self._nodes_executing[c]['dcall'] for c in callerids]
        return callers

    def node(self, id):
        return self._nodes_executing[id]

    def node_ids(self):
        return self._nodes_executing.keys()

    def executing_callee_ids(self, caller_id):
        return self._graph_executing.callees(caller_id)
    
    def executing_caller_ids(self, callee_id):
        return self._graph_executing.callers(callee_id)
        
if __name__ == "__main__":
    test_DNodeGraph()
    print('dnodedb.py test_DNodeGraph: OK')