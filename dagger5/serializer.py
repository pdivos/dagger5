from io import BytesIO
import pickle
from dagger5.core import DRef, is_primitive


def serialize(obj):
    """
    obj: object to serialize
    returns: bytes of serialized object
    """
    buffer = BytesIO()
    pickle.dump(obj, buffer)
    return buffer.getvalue()

def deserialize(buffer):
    """
    buffer: bytes containing the serialized object
    returns: deserialized object
    """
    buf = BytesIO(buffer)
    r = pickle.load(buf)
    buf.close()
    return r
