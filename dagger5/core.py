import enum
import dagger5.utils as utils

FRAME_SIZE = 128 * 1024 # * 1024  # maximum size of any single serialized object
                                # should match the memory capacity of a worker

class DStatus(enum.IntEnum):
    READY = 0           # ready to be evaluated, aka. it is on the work queue
    RUNNING = 1         # being evaluated, aka. has been picked up by a worker
    WAITING = 2         # has deps that are not ready, waiting for those to become available
    COMPLETED = 3       # evaluation successful
    # Note:
    # "executing" means READY, RUNNING or WAITING
    # "completed" means COMPLETEDE

def is_primitive(v):
    ret = (v is None or type(v) in (bool, float)) or \
          (type(v) == int and v >= -2**63 and v < 2**64)
    return ret

class DRef(bytes):
    "DRef is a key for the key value store which is an SHA1 of a bytes array"
    def __new__(cls, arg):
        assert type(arg) in (str, bytes, bytearray), "DRef must be initialized with str/bytes/bytearray: " + repr(arg)
        if type(arg) == str:
            assert len(arg) == 40, "DRef must be hexadecimal SHA1 value: " + repr(arg)
            return bytes.__new__(cls, bytes.fromhex(arg))
        else:
            assert len(arg) == 20, "DRef should be a 20 byte SHA1 value: " + repr(arg)
            return bytes.__new__(cls, bytes(arg))
    @staticmethod
    def from_buf(buf):
        assert type(buf) in (bytes, bytearray)
        from dagger5.utils import sha1
        return DRef(sha1(buf))
    def hex(self):
        return utils.hex(self)
    def __repr__(self):
        return self.__class__.__name__+'('+self.hex()+')'
    def __str__(self):
        return repr(self)
    def __getnewargs__(self):
        return (bytes(self),)

class DCall(tuple):
    """
    DCall is a function call:
        module: string name of module
        func: string name of function
        args: list of arguments containing primitives or DRef
        ts: timestamp
    """
    __slots__ = []
    def __new__(cls, ts, module, func, args):
        assert type(ts) is int
        assert type(module) is str
        assert type(func) is str
        if type(args) is list:
            args = tuple(args)
        assert type(args) is tuple
        for arg in args: assert is_primitive(arg) or type(arg) is DRef
        return tuple.__new__(cls, (ts, module, func, args))
    @property
    def ts(self):
        return tuple.__getitem__(self, 0)
    @property
    def module(self):
        return tuple.__getitem__(self, 1)
    @property
    def func(self):
        return tuple.__getitem__(self, 2)
    @property
    def args(self):
        return tuple.__getitem__(self, 3)
    def __repr__(self):
        reprargs = ','.join(map(repr, self.args))
        return self.__class__.__name__+'({}.{}({})@{})'.format(self.module, self.func, reprargs, self.ts)
    def __getnewargs__(self):
        return (self.ts,self.module,self.func,self.args)
    def to_json(self, dref_to_obj):
        return {
            'ts': self.ts,
            'module': self.module,
            'func': self.func,
            'args': [dref_to_obj[a] if a in dref_to_obj else a for a in self.args],
        }

class DNodeResult(tuple):
    """
    DNodeResult is the result of a completed node
    is_success == True: node succeeded, value contains the return value
    is_succedd == False: node failed with an exception, value contains stack trace
    """
    __slots__ = []
    def __new__(cls, is_success, value, stdout, stderr):
        assert type(is_success) is bool
        assert type(value) is DRef or is_primitive(value)
        assert type(stdout) is DRef or is_primitive(stdout)
        assert type(stderr) is DRef or is_primitive(stderr)
        return tuple.__new__(cls, (is_success, value, stdout, stderr))
    @property
    def is_success(self):
        return tuple.__getitem__(self, 0)
    @property
    def value(self):
        return tuple.__getitem__(self, 1)
    @property
    def stdout(self):
        return tuple.__getitem__(self, 2)
    @property
    def stderr(self):
        return tuple.__getitem__(self, 3)
    def __repr__(self):
        return self.__class__.__name__+'({},{},{},{})'.format(self.is_success, repr(self.value), repr(self.stdout), self.stderr)
    def __getnewargs__(self):
        return tuple(self)
    def to_json(self, dref_to_obj):
        return {
            'is_success': self.is_success,
            'value': dref_to_obj[self.value] if self.value in dref_to_obj else self.value,
            'stdout': dref_to_obj[self.stdout] if self.stdout in dref_to_obj else self.stdout,
            'stderr': dref_to_obj[self.stderr] if self.stderr in dref_to_obj else self.stderr,
            'children': self.children
        }

class DServerError(Exception):
    "thrown by server in case of an internal error"
    def __init__(self, message):
        super().__init__(message)

class DException(Exception):
    """
    if a client node fais with exception the stack trace is wrapped into this as a string along with the dnodekey of the node that failed
    this is sent as the result of the node
    when parents invoke a node that resulted with a DException, it will be thrown in the caller, upon calling the node that errored out
    """
    def __init__(self, stacktrace, dcalls):
        assert type(stacktrace) is str
        assert type(dcalls) is list
        for e in dcalls: assert type(e) is DCall
        super().__init__(stacktrace, dcalls)
    @property
    def stacktrace(self):
        return self.args[0]
    @property
    def dcalls(self):
        return self.args[1]
    def __repr__(self):
        return self.__class__.__name__+'\n{}\nDagger Stacktrace:\n{}'.format(self.stacktrace, '\n'.join([repr(e) for e in self.dcalls]))
    def __str__(self):
        return repr(self)

class DMissingDep(Exception):
    "this is thrown by DLambda decorator to signal a missing dependency"
    pass
