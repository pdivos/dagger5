from dagger5 import call, get_completed_nodes, get_completed_node_by_id, Globals, DepCollector

Globals.set_exception_handler(lambda stacktrace: print('exception_handler:', stacktrace))
# Globals.set_loglevel(1)

def fib(n):
    if n <= 1:
        return n
    else:
        DepCollector().require('dagger5.example','fib',n-1).require('dagger5.example','fib',n-2).request()
        return call('dagger5.example','fib',n-1)+call('dagger5.example','fib',n-2)

def fib_exception(n):
    if n > 1:
        return call('dagger5.example','fib_exception',n-1)+call('dagger5.example','fib_exception',n-2)
    else:
        raise Exception('Error happened')

def objects(obj):
    return {'a':obj,'b':'hello world'}

def test():
    cache='file:/usr/src/app/data'
    objects = call('dagger5.example','objects', {'a':1,'b':2}, cache=cache, ts = 0)
    if objects == {'a':{'a':1,'b':2},'b':'hello world'}:
        print('ok: objects')
    else:
        print('ERROR: ', objects)
    fib_5 = call('dagger5.example','fib', 5, cache=cache, ts = 0)
    if fib_5 == 5:
        print('ok: fib_5 == 5')
    else:
        print('ERROR: ', fib_5)
    try:
        call('dagger5.example','fib_exception', 5, cache=cache, ts = 0)
        print('ERROR: exception not thrown')
    except Exception as e:
        if 'Error happened' in str(e):
            print('OK: exception thrown')
        else:
            print('ERROR: ', e)
    i = 0
    for node in get_completed_nodes(cache=cache, include_failed=True):
        i += 1
    assert i == 12, i
    get_completed_node_by_id(node['id'], cache=cache)
    print('OK: all tests passed')

if __name__ == '__main__':
    test()
