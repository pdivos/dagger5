class MyZodb():
    """
    my wrapper for ZODB

    this takes another object which has ZODB persistable attributes such as LOBTree and stuff
    takes those members and puts them inside ZODB.
    also, if those members are already in ZODB then replaces the ones in the objects with the ones in ZODB
    this is what "binding" means in this context
    """
    def __init__(self, zodb_fname, cleanup_on_start = False):
        """
        """
        if zodb_fname is not None and cleanup_on_start:
            import os
            if os.path.exists(zodb_fname):
                for suffix in ['', '.index', '.lock', '.tmp']:
                    os.unlink(zodb_fname + suffix)
        # connecting to ZODB
        import ZODB
        import transaction as trans
        if zodb_fname is not None:
            import ZODB.FileStorage
            storage = ZODB.FileStorage.FileStorage(zodb_fname) # writes "Ignoring index for /data/test_zodb.fs" to stdout in case file doesn't exist, maybe a problem?
        else:
            storage = None                # in-memory ZODB for testing
        self.db = ZODB.DB(storage)
        self.transaction = trans.TransactionManager()
        self.conn = self.db.open(self.transaction)    # alternative: with db.transaction() as conn: this will give you a conn and also do transaction.commit() 
        self.root = self.conn.root()  # NOTE: conn.root also exists but that's a misleading partial convenience thing, although it's unclear what is the convenience
    
    def bind(self, obj, attrs_to_bind):
        """
        obj: some object that has ZODB persistable members that need to be persisted
        attrs_to_bind: list of members to bind and persist

        binding the persistent objects defined in the above attributes to ZODB
        if any of those not in db then it will be inserted with the default value above
        if in db, then attribute value will be overwritten from zodb
        essentially this will ensure that we can refer to persisted ZODB objects through self.xxx
        """
        for attr in attrs_to_bind:
            self._bind_attr_to_root(obj, attr, self.root)

    def _bind_attr_to_root(self, obj, attr, root):
        """
        binds attrs of self to self.root which is the ZODB root
        binding means that:
            if root has no attr then takes it from self which is supposed to contain the default value
            then sets self.attr to self.root[attr]
        attr should be a persistable object, therefore when we modify it through self.attr,
            the change will be picked up by ZODB
        can handle nested attrs separated by a dot
        """
        assert type(attr) is str
        import persistent
        v = obj
        attr_parts = attr.split('.')
        for a in attr_parts:
            v_prev = v
            v = getattr(v, a)
        if attr not in root:
            root[attr] = v
            assert isinstance(v, persistent.Persistent)
            # print(64, 'empty', attr)
        else:
            # print(66, 'found', attr, root[attr])
            pass
        setattr(v_prev, attr_parts[-1], root[attr])

    def commit(self):
        "commit changes"
        self.transaction.commit()

    def close(self):
        "call this before quitting so that everything is written to disk"
        self.db.close()

def test():
    class Test():
        def __init__(self):
            from BTrees.OOBTree import OOBTree
            self.x = OOBTree()
        def set(self):
            self.x['a'] = 'b'
        def get(self):
            return self.x.get('a')
    t = Test()
    m = MyZodb('/tmp/test.zodb')
    m.bind(t,['x'])
    print(t.get())
    t.set()
    print(t.get())
    m.commit()
    m.close()

if __name__ == "__main__":
    test()