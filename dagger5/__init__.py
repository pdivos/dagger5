from dagger5.core import DCall
from dagger5.core import DStatus
from dagger5.core import DException
from dagger5.core import DMissingDep
from dagger5.server import Dagger5Server

class Globals():
    """
    home for all globals held by the single-threaded dagger process
    """

    # "flag whether we are in local exec mode"
    # is_local_exec = False # if set to false, missig deps wont' be registered as missing. use for debugging only

    "dcall that is being executed right now, so we know what to register missing deps to"
    _dcall = None
    @staticmethod
    def set_dcall(dcall):
        assert type(dcall) is DCall or dcall is None
        Globals._dcall = dcall
    @staticmethod
    def get_dcall():
        return Globals._dcall

    "global client instance"
    _dagger5server = None
    @staticmethod
    def set_dagger5server(dagger5server):
        Globals._dagger5server = dagger5server
    @staticmethod
    def get_dagger5server():
        return Globals._dagger5server

    "a callback that will be called on each exception"
    _exception_handler = None
    @staticmethod
    def set_exception_handler(exception_handler):
        assert callable(exception_handler)
        Globals._exception_handler = exception_handler
    @staticmethod
    def get_exception_handler():
        return Globals._exception_handler

    "a callback that will be called on each exception"
    _loglevel = 0
    @staticmethod
    def set_loglevel(loglevel):
        assert type(loglevel) is int
        Globals._loglevel = loglevel
    @staticmethod
    def get_loglevel():
        return Globals._loglevel
    
def log(*msg):
    from dagger5.utils import log
    if Globals.get_loglevel() > 0:
        log(*msg)
    pass

def _callee(dagger5server, ts, module, func, args):
    "constructs callee dcall"
    assert type(args) in (list, tuple), str(type(args)) + "," + repr(args)
    args = [dagger5server.db.dref_ensure(a) for a in args]
    dcall = DCall(ts, module, func, args)
    log('_callee', ts, module, func, args, dcall)
    return dcall

from contextlib import contextmanager

@contextmanager
def _get_dagger5server(cache):
    if cache is not None:
        # running outside of a node, need to construct Dagger5Server
        dagger5server = Dagger5Server(cache)
        assert Globals.get_dagger5server() is None
    else:
        # runnign inside a dagger5 node, can use Global
        dagger5server = Globals.get_dagger5server()
        assert dagger5server is not None and type(dagger5server) is Dagger5Server
    yield dagger5server
    if cache is not None:
        dagger5server.shutdown()

def call(module, func, *args, **kwargs):
    """
    cache is the only allowed kwarg
    if cache is not None then constructs an internal Dagger5Server and does a synchronous call
    otherwise assumes that is running in a node and Dagger5Server instance is available in Globals
    """
    log('args', args)
    for k in kwargs.keys():
        assert k in ['cache','ts','throw']
    throw = kwargs.get('throw',True)
    ts = kwargs.get('ts', None)
    cache = kwargs.get('cache', None)
    if cache is not None:
        # running outside of a node, need to construct Dagger5Server
        dagger5server = Dagger5Server(cache)
        assert Globals.get_dagger5server() is None
        caller = None
        assert ts is not None
    else:
        # runnign inside a dagger5 node, can use Global
        dagger5server = Globals.get_dagger5server()
        assert dagger5server is not None and type(dagger5server) is Dagger5Server
        caller = Globals.get_dcall()
    if ts is None:
        ts = Globals.get_dcall().ts
    callee = _callee(dagger5server, ts, module, func, args)
    ret = dagger5server.call(callee, caller=caller)
    if ret['dstatus'] == DStatus.COMPLETED:
        is_success = ret['result'].is_success
        value = ret['result'].value
        value = dagger5server.db.dref_resolve(value)
        log('call', 'completed', is_success, func, args, caller, is_success, value)
        if cache is not None:
            dagger5server.shutdown()
        if is_success:
            # succeeded node return values are returned as normal
            return value
        else:
            # failed node return values contain stack trace, these are raised here to emulate exception
            assert type(value) is DException, type(value)
            log('call', 'raise', value)
            if throw:
                raise value
            else:
                return None
    else:
        if cache is None:
            # runnign inside a dagger5 node, need to raise MissingDep
            log('call', 'missing', func, args, caller)
            raise DMissingDep('missing dep: ' + repr(callee))
        else:
            # running outside of a node, need to sync call, that is start server until all is done
            dagger5server.serve_forever(stop_on_done = True)
            ret = dagger5server.call(callee, caller=caller)
            assert ret['dstatus'] == DStatus.COMPLETED
            is_success = ret['result'].is_success
            value = ret['result'].value
            value = dagger5server.db.dref_resolve(value)
            log('call', 'completed', is_success, func, args, caller, is_success, value)
            if is_success:
                # succeeded node return values are returned as normal
                return value
            else:
                # failed node return values contain stack trace, these are raised here to emulate exception
                assert type(value) is DException
                log('call', 'raise', value)
                if throw:
                    raise value
                else:
                    return None

def remove(module, func, *args, **kwargs):
    """
    removes a completed node from the cache
    """
    log('args', args)
    for k in kwargs.keys():
        assert k in ['cache','ts','throw']
    ts = kwargs.get('ts', None)
    cache = kwargs.get('cache', None)
    if cache is not None:
        # running outside of a node, need to construct Dagger5Server
        dagger5server = Dagger5Server(cache)
        assert Globals.get_dagger5server() is None
        caller = None
        assert ts is not None
    else:
        # runnign inside a dagger5 node, can use Global
        dagger5server = Globals.get_dagger5server()
        assert dagger5server is not None and type(dagger5server) is Dagger5Server
        caller = Globals.get_dcall()
    if ts is None:
        ts = Globals.get_dcall().ts
    callee = _callee(dagger5server, ts, module, func, args)
    return dagger5server.remove(callee)

class DepCollector(object):
    """
    DepCollector requests deps explicitly without waiting for the function to hit them.
    Use for requiring missing deps are in a loop.
    usage:
    DepCollector().require(func1, args1).require(func2, args2).request()
    """
    def __init__(self):
        self.deps = []
    def require(self, module, func, *args):
        assert type(func) is str
        assert type(args) in (tuple,list)
        ts = Globals.get_dcall().ts
        callee = _callee(Globals.get_dagger5server(), ts, module, func, args)
        self.deps.append(callee)
        return self
    def request(self):
        client = Globals.get_dagger5server()
        i = 0
        batch_size = 1000
        while True:
            deps = self.deps[(i*batch_size):((i+1)*batch_size)]
            if len(deps)==0:
                break
            caller = Globals.get_dcall()
            callees = deps
            if not client.call_multiple(callees, caller=caller):
                raise DMissingDep()
            i += 1

def get_completed_nodes(ts = None, module = None, func = None, include_failed = False, cache = None):
    with _get_dagger5server(cache) as dagger5server:
        nodes = dagger5server.get_completed_nodes(ts = ts, module = module, func = func, include_failed = include_failed)
    return nodes

def get_completed_node_by_id(id, cache = None):
    with _get_dagger5server(cache) as dagger5server:
        node = dagger5server.get_completed_node_by_id(id)
    return node

# def iter_deps(func, args, i_arg_to_map, list_of_inputs):
#     """
#     an efficient generation of deps and iteration through the results
#     this is preferred to DepCollector when possible because it's a cleaner interface but under the hood it's the same thing

#     runs DepCollector func(args) with subsituting to the i_arg_to_map.th arg the values of list_of_inputs
#     returns a list fo same size as list_of_inputs, containing the corresponding results
#     iter_deps('myfunc', ['a0',None,'a2'], 1, a1s)

#     i_arg_to_map can also be a list of inds in which case list_of_inputs is expected to be a list of tuples, usually created wit zip:
#     iter_deps('myfunc', ['a0',None,'a2','None], [1,3], zip(a1s, a3s))
#     """
#     if type(i_arg_to_map) is int:
#         assert i_arg_to_map>=0 and i_arg_to_map<len(args)
#         assert args[i_arg_to_map] is None
#     elif type(i_arg_to_map) is list:
#         for i in i_arg_to_map:
#             assert i >= 0 and i < len(args)
#             assert args[i] is None
#     else:
#         assert False, "incorrect type: " + str(i_arg_to_map)
#     assert type(list_of_inputs) is list
#     dc = DepCollector()
#     for inp in list_of_inputs:
#         if type(i_arg_to_map) is int:
#             args[i_arg_to_map] = inp
#         else:
#             assert len(inp) == len(i_arg_to_map), str(inp) + ", " + str(i_arg_to_map)
#             for i in range(len(i_arg_to_map)):
#                 args[i_arg_to_map[i]] = inp[i]
#         dc.require(func, args)
#     dc.request()
#     for inp in list_of_inputs:
#         if type(i_arg_to_map) is int:
#             args[i_arg_to_map] = inp
#         else:
#             assert len(inp) == len(i_arg_to_map), str(inp) + ", " + str(i_arg_to_map)
#             for i in range(len(i_arg_to_map)):
#                 args[i_arg_to_map[i]] = inp[i]
#         yield (inp, call(func, args))
